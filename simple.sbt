name := "spark-hipipe"

version := "1.0"

scalaVersion := "2.11.8"
//scalaVersion := "2.12.1"

autoScalaLibrary := false

//libraryDependencies += "org.apache.spark" %% "spark-core" % "1.2.0"
//libraryDependencies += "org.apache.spark" % "spark-core_2.10" % "2.2.0"
//libraryDependencies += "org.apache.spark" % "spark-streaming_2.10" % "2.2.0" % "provided"
libraryDependencies += "org.apache.spark" %% "spark-core" % "2.3.0"
libraryDependencies += "org.apache.spark" %% "spark-streaming" % "2.3.0" % "provided"
//libraryDependencies += "org.scala-lang" % "scala-library" % scalaVersion.value
