#!/bin/bash

SPARK_HOME=$1
R1=$2
R2=$3

if [ -z $SPARK_HOME ] || [ -z $R1 ] || [ -z $R2 ]; then
    echo "Argument error"
    exit
fi

#    --conf spark.local.dir=/mnt/test \
#    --master local[4]    
$SPARK_HOME/bin/spark-submit --class ibms.gp.bioinfo.runner.StreamingNamePipe --master spark://node101.gp.bioinfo:7077 \
    --conf spark.streaming.blockInterval=50 \
    --conf spark.executor.extrajavaoptions="Xmx6g" \
    --conf spark.driver.memory=4g \
    --conf spark.executor.memory=4g \
    spark-hipipe_2.11-1.0.jar $R1 $R2 500 &> qq.log
