### Resources
- /gfs/repo8_2/hipipe/data/1342/NA12878D_HiSeqX_R1_001.fastq.gz
- q.fastq.gz

```bash
$ pigz -dck /gfs/repo8_2/hipipe/data/1342/NA12878D_HiSeqX_R1_001.fastq.gz | head -n 8 | gzip -c > q.fastq.gz
```
### Example
```bash=
$ sbt package
$ ../spark-2.2.0-bin-hadoop2.7/bin/spark-submit --class "SimpleApp" --master local[4] \
	target/scala-2.10/simple-project_2.10-1.0.jar /mnt/repo2/bio_repo/sequence/test_NA12878_R1.fastq.gz
$ ../spark-2.2.0-bin-hadoop2.7/bin/spark-submit --class "ibms.gp.bioinfo.runner.MegaStats" \
	--master local[4] target/scala-2.10/simple-project_2.10-1.0.jar \
	/mnt/repo1/genepipe/NA12878-megaquery/vcf-2017-07-01GRCh38/vcf_variation_annoationGRCh38.csv
$ ../spark-2.2.0-bin-hadoop2.7/bin/spark-submit --class SimpleApp --master spark://node161.gp.bioinfo:7077 simple-project_2.10-1.0.jar /gfs/repo8_1/hipipe/data/2554/HG001-NA12878-50x_R1_.fastq.gz
```

### Test for basic fastq splitting and dispatch partition
```bash=
$ ../spark-2.2.0-bin-hadoop2.7/bin/spark-submit --class ibms.gp.bioinfo.runner.BashPipe --master local[4] target/scala-2.11/spark-hipipe_2.11-1.0.jar $PWD test.fastq.gz test_2.fastq.gz
```

### Test for streaming fastq
```bash=
$ ../../spark-2.2.0-bin-hadoop2.7/bin/spark-submit --class ibms.gp.bioinfo.runner.FastqStreaming --master spa://node163.gp.bioinfo:7077 --driver-memory 20g --executor-memory 20g tt.jar ./test.fastq &> kk.log
```

### Test for Netcat and CustomReceiver
```bash=
# netcat as tcp server
$ src/netcat -l -t -p 9999
# submit spark
$ ./../../spark/spark-2.2.0-bin-hadoop2.7/bin/spark-submit --class ibms.gp.bioinfo.example.CustomReExample --master spark://node163.gp.bioinfo:7077 tt.jar 192.168.104.161 9999
```

### Run single scala application
```bash=
java -cp target/scala-2.10/simple-project_2.10-1.0.jar:/home/s4553711/.sdkman/candidates/scala/current/lib/scala-library.jar ibms.gp.bioinfo.runner.Tutorial
```

### Custom Receiver Test
Test steaming fastq to spark streaming
```bash=
$ pigz -dck /home/s4554711/bio_repo/sequence/HG001-NA12878-50x_R1_.fastq.gz | head -n 40000 |  \
    ../../spark-2.2.0-bin-hadoop2.7/bin/spark-submit --class ibms.gp.bioinfo.runner.FastqStreaming \
    --master local[4] --conf spark.streaming.blockInterval=50 --conf spark.local.dir=/mnt/test \
    --conf spark.executor.extrajavaoptions="Xmx6g" --conf spark.driver.memory=4g --conf spark.executor.memory=4g \
    target/scala-2.11/spark-hipipe_2.11-1.0.jar HG001-NA12878-50x_R1_.fastq.gz &> rr.log
```

### Reading input with textFile and shuffle over all clusters
```bash
$ ../../spark-2.2.0-bin-hadoop2.7/bin/spark-submit --class ibms.gp.bioinfo.runner.BashPipe --master spark://node101.gp.bioinfo:7077 --driver-memory 40g --executor-memory 40g --executor-cores 1 --num-executors 8 --conf spark.dynamicAllocation.enabled=true --conf spark.shuffle.service.enabled=true --conf spark.dynamicAllocation.minExecutors=8 --conf spark.dynamicAllocation.initialExecutors=8 spark-hipipe_2.11-1.0.jar $PWD a.fastq.gz b.fastq.gz &> run3-4.log &
```

### Custom sreaming receiver with single or paired file receiver
date: 2018/04-16
```bash=
$ ../spark-2.2.0-bin-hadoop2.7/bin/spark-submit --class ibms.gp.bioinfo.runner.StreamingNamePipe \
    --master local[4] --conf spark.streaming.blockInterval=50 --conf spark.local.dir=/mnt/test \
    --conf spark.executor.extrajavaoptions="Xmx6g" --conf spark.driver.memory=4g --conf spark.executor.memory=4g \
    target/scala-2.11/spark-hipipe_2.11-1.0.jar test.fastq.gz test_2.fastq.gz &> qq.log
$ sbt package
```
