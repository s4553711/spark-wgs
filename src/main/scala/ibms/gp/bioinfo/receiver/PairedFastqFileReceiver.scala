package ibms.gp.bioinfo.receiver

import ibms.gp.bioinfo.util.FileIterator
import org.apache.spark.internal.Logging
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.receiver.Receiver

/**
  * Created by s4553711 on 2018/3/31.
  */
class PairedFastqFileReceiver (file: String, file2: String) extends
    Receiver[((String, String, String, String),(String, String, String, String))](StorageLevel.MEMORY_AND_DISK_2) with Logging {
    override def onStart() {
        new Thread("PairedFastq Receiver") {
            override def run() { receive() }
        }.start()
    }

    override def onStop() {}

    private def receive(): Unit = {
        try {
            val it1: FileIterator = new FileIterator(file)
            val it2: FileIterator = new FileIterator(file2)
            while(!isStopped && it1.hasNext && it2.hasNext) {
                store((it1.next, it2.next))
            }
            it1.close
            it2.close
        } catch {
            case e: Exception => e.printStackTrace()
        } finally {
            println("Stopped receiving")
            onStop()
        }
    }
}
