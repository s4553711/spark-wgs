package ibms.gp.bioinfo.receiver

import ibms.gp.bioinfo.util.FileIterator
import org.apache.spark.internal.Logging
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.receiver.Receiver

/**
  * Created by s4553711 on 2018/3/27.
  */
class FastqFileReceiver (file: String) extends Receiver[(String, String, String, String)](StorageLevel.MEMORY_AND_DISK_2) with Logging {
    override def onStart() {
        new Thread("Fastq Receiver") {
            override def run() { receive() }
        }.start()
    }

    override def onStop() {}

    private def receive(): Unit = {
        try {
            val iterator: FileIterator = new FileIterator(file)
            while(!isStopped && iterator.hasNext) {
                val k = iterator.next()
                store(k)
            }
            iterator.close
        } catch {
            case e: Exception => e.printStackTrace()
        } finally {
            println("Stopped receiving")
            onStop()
        }
    }
}
