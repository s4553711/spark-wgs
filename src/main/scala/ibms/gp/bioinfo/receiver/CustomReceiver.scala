package ibms.gp.bioinfo.receiver

import java.io.{BufferedReader, InputStreamReader}
import java.net.Socket
import java.nio.charset.StandardCharsets

import org.apache.spark.internal.Logging
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.receiver.Receiver

/**
  * Created by s4553711 on 2018/1/31.
  */
class CustomReceiver (host: String, port: Int) extends Receiver[String](StorageLevel.MEMORY_AND_DISK_2) with Logging {
    override def onStart(): Unit = {
        println(">> 1 Receiver Start")
        logInfo(">> 2 Receiver Start")
        new Thread("Socket Receiver") {
            override def run() { receive() }
        }.start()
    }

    override def onStop(): Unit = {}

    private def receive() {
        var socket: Socket = null
        var userInput: String = null
        try {
            // Connect to host:port
            socket = new Socket(host, port)

            // Until stopped or connection broken continue reading
            val reader = new BufferedReader(
                new InputStreamReader(socket.getInputStream(), StandardCharsets.UTF_8))
            userInput = reader.readLine()
            while(!isStopped && userInput != null) {
                store(userInput)
                userInput = reader.readLine()
            }
            reader.close()
            socket.close()

            // Restart in an attempt to connect again when server is active again
            restart("Trying to connect again")
        } catch {
            case e: java.net.ConnectException =>
                // restart if could not connect to server
                restart("Error connecting to " + host + ":" + port, e)
            case t: Throwable =>
                // restart if there is any other error
                restart("Error receiving data", t)
        }
    }
}
