package ibms.gp.bioinfo.receiver

import java.io.{BufferedReader, FileReader, IOException, InputStreamReader}

import ibms.gp.bioinfo.util.StdIterator
import org.apache.spark.internal.Logging
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.receiver.Receiver

/**
  * Created by s4553711 on 2018/1/29.
  */
class FastqReceiver(file: String) extends Receiver[(String, String, String, String)](StorageLevel.MEMORY_AND_DISK_2) with Logging{
    override def onStart() {
        new Thread("Fastq Receiver") {
            override def run() { receive() }
        }.start()
    }

    override def onStop() {}

    private def receive(): Unit = {
        try {
            val iterator:StdIterator = new StdIterator
            while(!isStopped && iterator.hasNext) {
                val k = iterator.next()
                store(k)
                println(k._1)
            }
            iterator.closeStream
        } catch {
            case e: Exception => e.printStackTrace()
        } finally {
            println("Stopped receiving")
            onStop()
        }
    }

    private def receive7788(): Unit = {
        var reader: BufferedReader = null
        try {
            reader = new BufferedReader(new InputStreamReader(System.in))
            var result = List[(String, String, String, String)]()
            var rec = reader.readLine()
            var interval: Int = 0
            while (rec != null) {
                result.::=(rec, reader.readLine(), reader.readLine(), reader.readLine)
                interval += 1
                if (interval % 160 == 0) {
                    //store(result.iterator)
                    interval = 0
                    result = Nil //List[(String, String, String, String)]()
                }
                rec = reader.readLine()
            }
            println("close")
        } catch {
            case e: IOException => println(s"Error while reading from stdin")
            case e: Exception => println(s"Exception error")
        } finally {
            reader.close()
        }
    }

    private def receive0(): Unit = {
        var reader: BufferedReader = null
        try {
            reader = new BufferedReader(new InputStreamReader(System.in))
            var rec = reader.readLine()
            var interval: Int = 0
            var cur: String = ""
            while (rec != null) {
                cur += rec + "\n" + reader.readLine() + "\n" + reader.readLine() + "\n" + reader.readLine() + "\n"
                interval += 1
                if (interval % 4 == 0) {
                    var result = List[(String)]()
                    result.::=(cur)
                    //store(result.iterator)
                    cur = ""
                    interval = 0
                }
                rec = reader.readLine()
            }
            println("close")
        } catch {
            case e: IOException => println(s"Error while reading from stdin")
            case e: Exception => println(s"Exception error")
        } finally {
            reader.close()
        }
    }

    private def receive2() {
        var rec: String = null
        try {
            val reader = new BufferedReader((new FileReader(file)))
            rec = reader.readLine()
            while(!isStopped && rec != null) {
//                store(rec)
                rec = reader.readLine()
            }
            reader.close()
        } catch {
            case e: IOException => println(s"Error while opening file $file")
            case e: Exception => println(s"Exception error")
        }
    }
}
