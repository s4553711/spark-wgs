package ibms.gp.bioinfo.util

import java.io.{BufferedReader, InputStreamReader}

/**
  * Created by s4553711 on 2018/2/14.
  */
class StdIterator extends Iterator[(String, String, String, String)] {
//class StdIterator[T] extends Iterator[T] {
    val input: BufferedReader = {
        new BufferedReader(new InputStreamReader(System.in))
    }
    private var nextLine: (String, String, String, String) = null

    override def hasNext: Boolean = {
        nextLine = (input.readLine(), input.readLine(), input.readLine(), input.readLine())
        nextLine != (null, null, null, null)
    }

    override def next(): (String, String, String, String) = {
//        val result = {
//            if (nextLine == null) (input.readLine(), input.readLine(), input.readLine(), input.readLine())
//            else try nextLine finally nextLine = null
//        }
//        if (result == null) Iterator.empty.next
//        else result
        if (nextLine == (null,null,null,null)) Iterator.empty.next()
        else nextLine
    }

    def closeStream(): Unit = {
        input.close()
    }
}
