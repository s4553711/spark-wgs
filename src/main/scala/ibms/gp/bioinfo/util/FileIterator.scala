package ibms.gp.bioinfo.util

import java.io.{BufferedInputStream, BufferedReader, FileInputStream, InputStreamReader}
import java.util.zip.GZIPInputStream

/**
  * Created by s4553711 on 2018/3/27.
  */
class FileIterator(file: String) extends Iterator[(String, String, String, String)]{
    //val input = Source.fromFile(file)
    val input: BufferedReader = {
        new BufferedReader(new InputStreamReader(new GZIPInputStream(new BufferedInputStream(new FileInputStream(file)))))
    }
    var nextLine: (String, String, String, String) = null

    override def hasNext: Boolean = {
        nextLine = (input.readLine, input.readLine, input.readLine, input.readLine)
        nextLine != (null, null, null, null)
    }


    override def next(): (String, String, String, String) = {
        if (nextLine == (null,null,null,null)) Iterator.empty.next()
        else nextLine
    }

    def close = {
        input.close()
    }
}
