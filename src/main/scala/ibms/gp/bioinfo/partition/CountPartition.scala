package ibms.gp.bioinfo.partition

import org.apache.spark.Partitioner

/**
  * Created by s4553711 on 2017/10/6.
  */
class CountPartition(numParts: Int) extends Partitioner {
    override def numPartitions: Int = numParts

    override def getPartition(key: Any): Int = {
        //println("PPPP: "+key.asInstanceOf[Int] % numPartitions)
        key.asInstanceOf[Int] % numPartitions
    }

    override def equals(other: Any): Boolean = other match {
        case iteblog: CountPartition =>
            iteblog.numPartitions == numPartitions
        case _ =>
            false
    }

    override def hashCode: Int = numPartitions
}
