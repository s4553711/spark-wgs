package ibms.gp.bioinfo.partition

/**
  * Created by s4553711 on 2017/10/13.
  */
object fastqPartition {
    def splitByUnit(x: Iterator[String]): Iterator[(Int, (String, String, String, String))] = {
        var result = List[(Int, (String, String, String, String))]()
        var counter:Int = 1
        while (x.hasNext) {
            val cur = (x.next(), x.next(), x.next(), x.next())
            result.::=(counter, cur)
            counter += 1
        }
        result.iterator
    }
    def createPair(x: Iterator[(String, String, String, String)]): Iterator[(String, (String, String, String, String))] = {
        var result = List[(String, (String, String, String, String))]()
        var counter: Int = 1
        while (x.hasNext) {
            // result.::=(counter, x.next())
            val content = x.next
            val header = content._1.split(" ")(0)
            result.::=(header, content)
            counter += 1
        }
        result.iterator
    }
}
