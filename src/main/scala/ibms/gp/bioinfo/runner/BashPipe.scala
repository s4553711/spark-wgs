package ibms.gp.bioinfo.runner

import java.io.PrintWriter
import java.net.InetAddress

import ibms.gp.bioinfo.partition.fastqPartition
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by s4553711 on 2018/1/17.
  */
object BashPipe {
    //val pipe: RandomAccessFile = new RandomAccessFile("/home/s4553711/tmp/spark-test/scala-src/q2", "rw")

    def sendToPipe(cmd: String): Unit = {
        println("called "+cmd)
        //        pipe.writeBytes(cmd)
        //        val out = new PrintWriter("/home/s4553711/tmp/spark-test/scala-src/qPipe")
        //        out.println(cmd)
        //        out.close()
    }

    def main(args: Array[String]): Unit = {
        val conf = new SparkConf().setAppName("BashPipe")//.set("spark.executor.cores", "2")
        val sc = new SparkContext(conf)
        //val ssc = new StreamingContext(conf, Seconds(1))

        val addr = InetAddress.getLocalHost
        val workDir = args(0)
        val input = args(1)
        val input2 = args(2)

        println("log > workDir: "+workDir)

        val pipe = new PrintWriter(workDir+"/q1")
        val pipe2 = new PrintWriter(workDir+"/q2")

        // test source 1
//        val data = List.range(1, 1000000)
//        val dataRDD = sc.makeRDD(data).repartition(2)
        // test source 2
        val dataRDD = sc.textFile(input, 1)
        val dataRDD2 = sc.textFile(input2, 1)

        // option 1
        //val pipeRDD2 = dataRDD.pipe("/home/s4553711/tmp/spark-test/scala-src/qq.sh")
//        val pipeRDD = dataRDD.mapPartitionsWithIndex((index,itr) => {
//            itr.toList.map(x => {
//                pipe.write(index+"\t"+x.toString+"\n")
//                pipe.flush()
//            }).iterator
//        })
//        pipeRDD.collect()

//        option 2
//        val pipeRDD = dataRDD.mapPartitions(fastqPartition.splitByUnit)
//            .repartition(2)
//            .mapPartitionsWithIndex((index, itr) => {
//            itr.toList.map(x => {
//                if (index == 0) {
//                    pipe.write(index + "\t" + x._1 + "\t" + addr.getHostName() + "\t" + x._2 + "\n")
//                    pipe.flush()
//                } else {
//                    pipe2.write(index+"\t" + x._1 + "\t" + addr.getHostName() + "\t" + x._2 + "\n")
//                    pipe2.flush()
//                }
//            }).iterator
//        })

        val r1 = dataRDD.mapPartitions(fastqPartition.splitByUnit)//.repartition(2)
        val r2 = dataRDD2.mapPartitions(fastqPartition.splitByUnit)//.repartition(2)
        val pipeRDD = r1.zip(r2).repartition(8).mapPartitionsWithIndex((index, itr) => {
            itr.toList.map(x => {
                if (index == 0) {
                    //println("i11 > " + index + " : " + x._1._2)
                    //println("i12 > " + index + " : " + x._2._2)
                    //pipe.write(" > " + index + "\t" + x._1._2._1 + "\t" + x._2._2._1 + "\n")
                    //pipe.write(" > " + index + "\n")
                    //pipe.flush()
                    x._1._2._1+"\n"+x._1._2._2+"\n"+x._1._2._3+"\n"+x._1._2._4+"\n"+
                        x._2._2._1+"\n"+x._2._2._2+"\n"+x._2._2._3+"\n"+x._2._2._4
                    //index + " ::: " + x._1._1 + " ::: " + x._2._2
                } else {
                    //println("i21 > " + index + " : " + x._1._2)
                    //println("i22 > " + index + " : " + x._2._2)
                    //pipe2.write(" > " + index+"\t" + x._1._2._1 + "\t" + x._2._2._1 + "\n")
                    //pipe2.write(" > " + index + "\n")
                    //pipe2.flush()
                    x._1._2._1+"\n"+x._1._2._2+"\n"+x._1._2._3+"\n"+x._1._2._4+"\n"+
                        x._2._2._1+"\n"+x._2._2._2+"\n"+x._2._2._3+"\n"+x._2._2._4
                    //index + " ::: " + x._1._1 + " ::: " + x._2._2
                }
            }).iterator
        }).pipe(workDir+"/qq.sh"+" "+workDir)

        //pipeRDD.take(1)
        pipeRDD.collect()
        //pipe.close()
        //pipe2.close()

        println("dataRDD> "+dataRDD.getNumPartitions)
        println("pipeRDD> "+pipeRDD.getNumPartitions)
        println("end")
    }
}
