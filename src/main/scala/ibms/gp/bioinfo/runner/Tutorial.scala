package ibms.gp.bioinfo.runner

import ibms.gp.bioinfo.util.StdIterator

import scala.io.StdIn
import java.io.{BufferedReader, InputStreamReader}

/**
  * Created by s4553711 on 2018/1/16.
  */
object Tutorial {
    def main(args: Array[String]): Unit = {
        println("this is a test")
        val list = List(1,2,3,4)
        for(i <- 0 until(list.length)) {
            println(i)
        }
        (list.::(5)).foreach(println)
        println("concate two lists")
        (list ::: list).foreach(println)
        CallSTdIterator
    }

    def BuiltInJavaStdIO: Unit = {
        //val br: BufferedReader = null
        val br = new BufferedReader(new InputStreamReader(System.in))
        var line = ""
        while({line = br.readLine(); line != null}) {
            println("call next: "+line)
        }
        br.close()
    }

    def BuiltInStdIO: Unit = {
        var k: String = ""
        while({k = StdIn.readLine(); k != null}) {
            println("call next: "+k)
        }
    }

    def CallSTdIterator: Unit = {
        val iterator:StdIterator = new StdIterator
        //val iterator = stdin.toIterator
        while(iterator.hasNext) {
            println("call next: "+iterator.next())
        }
    }
}
