package ibms.gp.bioinfo.runner

import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by s4553711 on 2017/7/28.
  */
object MegaStats {
    def main(args: Array[String]) {
        val input = args(0)
        val conf = new SparkConf().setAppName("Simple Application")
        val sc = new SparkContext(conf)

        val textFile = sc.textFile(input, 2)
        val clinVarRows = textFile.map(line => line.split("\",\"")).map(c => c(41)).filter(c => !c.equals(""))
        val numOfClinVar = clinVarRows.distinct().count()
        println("Lines with clinVar: %s".format(numOfClinVar))
    }
}
