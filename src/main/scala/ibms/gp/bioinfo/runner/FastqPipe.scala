package ibms.gp.bioinfo.runner

import ibms.gp.bioinfo.partition.{CountPartition, fastqPartition}
import org.apache.spark._

/**
  * Created by s4553711 on 2017/9/18.
  */
object FastqPipe {
    def main(args: Array[String]): Unit = {
        val input = args(0)
        val fPart = if (args.size > 1) args(1).toInt else 4
        val conf = new SparkConf().setAppName("FastqPipe")
        val sc = new SparkContext(conf)

        println("Log > input: "+input)
        println("Log > partition: "+fPart)

        val fastqs = sc.textFile(input, 2)
//        val result = fastqs.map(n => {
//            n
//        })
//        println("size> "+result.partitions.size)
//        println("total fastqs lines 1:"+fastqs.count())

        println("End reading fastq")

        val fastqRecords = fastqs.mapPartitions(fastqPartition.splitByUnit)
//        val fastqRecords = fastqs.mapPartitions{ x => {
//            var result = List[(Int, (String, String, String, String))]()
//            var counter:Int = 1
//            while (x.hasNext) {
//                val cur = (x.next(), x.next(), x.next(), x.next())
//                result.::=(counter, cur)
//                counter += 1
//            }
//            result.iterator
//        }}
//        fastqRecords.foreach(f => {
//            println("C: "+f._1)
//            println("G: "+f._2._1)
//        })

        println("End partition")

        val partFastqs = fastqRecords.partitionBy(new CountPartition(fPart))
        println("End partitionBy")
        val l = partFastqs.glom().map(_.length).collect()
        println(l.min, l.max, l.sum/l.length, l.length)
        println(partFastqs.partitions.size)

//        fastqRecords.foreach(fq => {
//            println("fastq: "+fq._1)
//        })
//        println("size> "+fastqRecords.partitions.size)
//        println("total fastqs lines 2: "+fastqRecords.count())
//        val indexFastqs = fastqRecords.mapPartitionsWithIndex{(index, it) => {
//            var result = List[(Int, (String, String, String, String))]()
//            var counter:Int = index
//            while(it.hasNext) {
//                println(">> index:"+index+", counter:"+counter)
//                result.::=(counter, it.next())
//                counter+=1
//            }
//            result.iterator
//        }}
//        indexFastqs.foreach(k => {
//            println("Final > "+k._2._2)
//        })
//        println("size: "+indexFastqs.partitions.size)
//        println("total fastq lines 3: "+indexFastqs.count())
    }
}
