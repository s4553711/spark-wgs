package ibms.gp.bioinfo.runner

import ibms.gp.bioinfo.partition.fastqPartition
import ibms.gp.bioinfo.receiver.{FastqFileReceiver, PairedFastqFileReceiver}
import org.apache.spark.SparkConf
import org.apache.spark.streaming.{Milliseconds, StreamingContext}

/**
  * Created by s4553711 on 2018/3/27.
  */
object StreamingNamePipe {
    def main(args: Array[String]): Unit = {
        println("Streaming Name Pipe start")
        val conf = new SparkConf().setAppName("FastqStreaming NamePipe")
        val batchSize = if(args.size == 3) args(2).toInt else 500
        println("batchSize: "+batchSize)
        //val ssc = new StreamingContext(conf, Milliseconds(500))
        val ssc = new StreamingContext(conf, Milliseconds(batchSize))

        loadPaired(ssc, args)

        ssc.start()
        ssc.awaitTermination()
        ssc.stop()
    }

    def loadPaired(ssc: StreamingContext, args: Array[String]): Unit = {
        val source = ssc.receiverStream(new PairedFastqFileReceiver(args(0), args(1)))
        source.foreachRDD(rdd => {
            val ct = rdd.map(k => {
                //println("R1 header: " + k._1._1)
                //println("R2 header: " + k._2._1)
                k
            }).count()
            println("Finish " + ct)
        })
    }

    def loadSingle(ssc: StreamingContext, args: Array[String]): Unit = {
        val fastqR1 = ssc.receiverStream(new FastqFileReceiver(args(0)))
        val fastqR2 = ssc.receiverStream(new FastqFileReceiver(args(1)))

        val r1Pair = fastqR1.mapPartitions(fastqPartition.createPair).window(Milliseconds(500))
        val r2Pair = fastqR2.mapPartitions(fastqPartition.createPair).window(Milliseconds(500))

         // Count the element in each stream
//         r1Pair.foreachRDD(rdd => {
//            val ct = rdd.map(d => {
//                println("Reads ::: " + d._2._1)
//            }).count()
//             println("final: "+ct)
//         })

        r1Pair.join(r2Pair).foreachRDD(rdd => {
            val ct = rdd.map(f => {
                println("Reads ::: " +  f._2._1._1 + " ::: " + f._2._2._1)
            }).count()
            println("final: "+ct)
        })
    }
}
