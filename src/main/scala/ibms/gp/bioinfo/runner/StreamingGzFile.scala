package ibms.gp.bioinfo.runner

import org.apache.spark.SparkConf
import org.apache.spark.streaming.{Milliseconds, StreamingContext}

/**
  * Created by s4553711 on 2018/2/22.
  */
object StreamingGzFile {
    /*
        This implement is used to demo how to monitor file added or changed and processes it with spark.
        The first argument is the folder path which indiciates where we are going to monitor.
     */
    def main(args: Array[String]): Unit = {
        println("Streaming start")
        val conf = new SparkConf().setAppName("GzStreaming").set("spark.executor.cores", "4")
        val ssc = new StreamingContext(conf, Milliseconds(500))
        val lines = ssc.textFileStream(args(0))
        val counts = lines.flatMap(_.split("\n")).map(line => {
            println("line > "+line)
        })
        counts.print()
        ssc.start()
        ssc.awaitTermination()
        println("Streaming End")
    }
}
