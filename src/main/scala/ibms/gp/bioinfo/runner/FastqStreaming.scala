package ibms.gp.bioinfo.runner

import ibms.gp.bioinfo.receiver.FastqReceiver
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.{Milliseconds, StreamingContext}

import scala.collection.mutable

/**
  * Created by s4553711 on 2018/1/29.
  */
object FastqStreaming {
    def main(args: Array[String]): Unit = {
        println("Streaming start")
        val conf = new SparkConf().setAppName("FastqStreaming").set("spark.executor.cores", "4")//.set("spark.streaming.blockInterval", "500")
        val ssc = new StreamingContext(conf, Milliseconds(500))
        val rddQueue = new mutable.Queue[RDD[String]]()

        //        lines.foreachRDD( rdd => {
        //            println("rdd .. "+rdd)
        //            rdd.foreachPartition( line => {
        //                println("partition .. "+line)
        //                line.foreach{ item =>
        //                    println("we get .. "+item)
        //                }
        //            })
        //        })

        // original 1
        val lines = ssc.receiverStream(new FastqReceiver(args(0)))
        //        val op = lines.map(k => {
//            println("Receive>>>\n"+k)
//            println("\n")
//        })
//        op.print()

        // for iterator[String]
//        val counts = lines.flatMap(line => line.split("\n")).filter(header => header.startsWith("@HWI")).map(k => {
//            //println("line > "+k)
//        })
//        counts.print()

        val counts = lines/*.filter(header => header._1.startsWith("@HWI")).map(k => {
            if (k._1 != null) println("line > "+k._1)
            k._1
        })*/.foreachRDD(rrd => {
            rrd/*.mapPartitionsWithIndex((index, itr)=>{
                itr.toList.map(x => {
                    index+","+x._1
                }).iterator
            })*/.repartition(1).map(i => {
                // println("oo: "+i._1)
                i
            }).pipe("/home/s4553711/tmp/spark-test/scala-src/batch.sh abc").collect()
        })
        //counts.print(10000)

//        val wordCounts = words.map(x => (x, 1)).reduceByKey(_ + _)
//        wordCounts.print()
//        words.foreachRDD(rdd => {
//            println("Num of rdd: "+rdd.partitions.length)
//            rdd.foreachPartition(line => {
//                println("each partition: "+line)
//                line.foreach(item => {
//                    println("item > "+item)
//                })
//            })
//        })
//        println(">>>> total: "+wordCounts.print())

        ssc.start()
        println("The result line count is : "+counts)
        ssc.awaitTermination()
        println("Streaming End")
//
//        // option 2
//        val inputStream = ssc.queueStream(rddQueue)
//        val reader = new BufferedReader((new FileReader(args(0))))
//        val words = inputStream.flatMap(_.split(" "))
//        val wordCounts = words.map(x => (x, 1)).reduceByKey(_ + _)
//        words.foreachRDD(rdd => {
//            println("Num of rdd: "+rdd)
//        })
//        println(">>>> total: "+wordCounts.print())
//        ssc.start()
//        println("file: "+args(0)+" open")
//        var rec = reader.readLine()
//        var tmp = List[String]()
//        var i: Int = 0
//        while(rec != null) {
//            tmp.::=(rec)
//            i += 1
//            if (i % 80 == 0) {
//                rddQueue.synchronized {
//                    //println("Append .. /"+i+", with tmp.size(): "+tmp.size)
//                    rddQueue += ssc.sparkContext.makeRDD(tmp)
//                }
//                i = 0
//                tmp = List()
//                //Thread.sleep(500)
//            }
//            rec = reader.readLine()
//        }
//        reader.close()
//        println("file: "+args(0)+" close")
//        while(!rddQueue.isEmpty) {
//            println("Wait Until End .. "+rddQueue.length)
//            Thread.sleep(1000)
//        }
//        ssc.stop()
//        println("Streaming End")
    }
}
