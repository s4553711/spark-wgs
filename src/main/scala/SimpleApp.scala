/* SimpleApp.scala */

import org.apache.spark.{SparkConf, SparkContext}

object SimpleApp {
    def main(args: Array[String]) {
        val logFile = args(0)
        val conf = new SparkConf().setAppName("Simple Application")
        val sc = new SparkContext(conf)
        //val logData = sc.textFile(logFile, 2).cache()
        val logData = sc.textFile(logFile, 2)
        //val numAs = logData.filter(line => line.contains("a")).count()
        //val numAs = logData.count()
        val numAs = logData.flatMap(line => line.split(" ")).map(word => (word, 1)).reduceByKey(_ + _)
        println("Lines with a: %s".format(numAs))
    }
}
